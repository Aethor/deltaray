extends Node

var renderer = null

var pause = false
var started = false

var http = preload("res://scenes/utils/http.gd").new()

func _ready():
	get_tree().set_auto_accept_quit(false)

func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		http.request(HTTPClient.METHOD_POST, "/shutdown", 204)
		get_tree().quit()

func toggle_pause():
	self.pause = not self.pause

func start_simulation(command, args, analyse_path, render):
	OS.execute(command, args, false)
	OS.delay_msec(1000)

	if render == "3d":
		renderer = load("res://scenes/Rendering/Rendering.tscn").instance()
		get_node("Viewport").add_child(renderer)
	elif render == "2d":
		renderer = load("res://scenes/Rendering/Rendering2D.tscn").instance()
		get_node("Viewport").add_child(renderer)
	else:
		print("[ERROR] Wrong render type : " + render)
		get_tree().quit()

	var answer = http.request(
		HTTPClient.METHOD_POST,
		"/simulation?path=" + analyse_path,
		201
	)

	if answer == null:
		print("MainController : Error : could not create simulation server-side")
		return

	var characters_datas = parse_json(answer)

	for data in characters_datas["positions"]:
		if render == "3d":
			self.renderer.create_character(
					data["character"],
					data["position"]["x"],
					data["position"]["y"],
					data["position"]["z"]
			)
		else:
			self.renderer.create_character(
					data["character"],
					data["position"]["x"],
					data["position"]["y"]
			)
		var character_button = load("res://scenes/UI/CharacterFollowButton.tscn").instance()
		character_button.text = data["character"]
		character_button.name = data["character"]
		self.get_node("UI/CharactersPanel/VBoxContainer/ButtonContainer").add_child(character_button)

	self.started = true

func stop_simulation():
	if not self.started or self.renderer == null:
		return

	self.renderer.queue_free()
	http.request(HTTPClient.METHOD_POST, "/shutdown", 204)
	var button_container = get_node(
		"UI/CharactersPanel/VBoxContainer/ButtonContainer")
	for button in button_container.get_children():
		button_container.remove_child(button)
	self.started = false
		
		
func get_next_simulation_frame(delta):
	var answer = http.request(
		HTTPClient.METHOD_POST,
		"/simulation/next_frame?delta=" + str(delta),
		200
	)

	if answer == null:
		print("MainController : Error : could not get next frame")
		return null

	return parse_json(answer)

func _process(delta):
	if not self.started or self.pause:
		return

	# Renderer instructions
	var frame_datas = self.get_next_simulation_frame(delta)
	if frame_datas != null:
		for position_update in frame_datas["positionUpdates"]:
			self.renderer.move_character(
				position_update["character"],
				position_update["positionUpdate"]["x"],
				position_update["positionUpdate"]["y"],
				position_update["positionUpdate"]["z"]
			)
		for relation_update in frame_datas["relationUpdates"]	:
			self.renderer.update_relation(
				relation_update["character1"],
				relation_update["character2"],
				relation_update["influenceUpdate"]
			)

	# UI Instructions
	var progression_slider = self.get_node("UI/ControlPanel/ControlBox/ProgressionSlider")
	var raw_progression_datas = http.request(
		HTTPClient.METHOD_GET,
		"/simulation/progression",
		200
	)
	if raw_progression_datas != null:
		var current_progression_datas = parse_json(raw_progression_datas)
		progression_slider.value = progression_slider.max_value * current_progression_datas["progression"]


