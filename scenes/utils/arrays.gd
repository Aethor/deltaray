func map(array: Array, lambda: FuncRef) -> Array:
    var new_array = [] 
    for value in array:
        new_array.append(lambda.call_func(value))
    return new_array