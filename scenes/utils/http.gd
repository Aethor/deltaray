func request(method, url, expected_code):

	var http_client = HTTPClient.new()

	http_client.connect_to_host("127.0.0.1", 8000)
	while http_client.get_status() == HTTPClient.STATUS_CONNECTING or http_client.get_status() == HTTPClient.STATUS_RESOLVING:
		http_client.poll()
		OS.delay_msec(1)

	if http_client == null:
		print("MainController : Warning : attempted to make an HTTP request with initialized client")
		return null
	if http_client.get_status() == HTTPClient.STATUS_DISCONNECTED:
		print("MainController : Warning : attempted to make an HTTP request with disconnected client")
		return null

	http_client.request(method, url, [])

	while http_client.get_status() == HTTPClient.STATUS_REQUESTING:
		http_client.poll()
		if not OS.has_feature("web"):
			OS.delay_msec(1)
		else:
			yield(Engine.get_main_loop(), "idle_frame")

	if not http_client.has_response():
		print("MainController : Error : couldn't get server answer")
		return null

	var code = http_client.get_response_code()
	if code != expected_code:
		print("MainController : Error : request to server failed with " + str(code))
		return null

	var buffer = PoolByteArray()
	while http_client.get_status() == HTTPClient.STATUS_BODY:
		http_client.poll()
		var chunk = http_client.read_response_body_chunk()
		if chunk.size() == 0:
			OS.delay_msec(1)
		else:
			buffer = buffer + chunk

	return buffer.get_string_from_ascii()

