extends FileDialog

func _ready():
	self.connect("file_selected", self, "_open_file")

func _open_file(self, path):
	self.get_parent().start_simulation(path)
