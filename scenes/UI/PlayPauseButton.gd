extends Button

func _ready():
	pass 

func _pressed():
	get_tree().get_root().get_node("MainController").toggle_pause()
	self.text = "\u25B6" if self.text == "||" else "||"

