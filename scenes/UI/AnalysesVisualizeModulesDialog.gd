extends PopupMenu

var available_modules = {}

func _ready():
	var file = File.new()
	file.open("res://scripts/core/modules.json", file.READ)
	var modules_datas = parse_json(file.get_as_text())
	file.close()

	for module in modules_datas:
		self.available_modules[module["name"]] = module
		self.add_item(module["name"])

	#TODO: implement custom module path selection
	self.add_item("Custom module... (unimplemented)")

	self.connect("id_pressed", self, "_on_item_pressed")

func _on_item_pressed(ID):
	var item_name = self.get_item_text(ID)

	if item_name in available_modules:

		var module = available_modules[item_name]
		var module_path = ProjectSettings.globalize_path(
			module["path"])
		var module_command = module["command"].replace(
			"${path}", module_path)
		var module_args = module["args"]
		for i in range(0, module_args.size()):
			module_args[i] = module_args[i].replace(
				"${path}", module_path)

		var options_dialog = get_node("../AnalysesOptionsDialog")
		options_dialog.module = {
			"name": module["name"],
			"path": module_path,
			"command": module_command,
			"args": module_args,
			"render": module["render"]
		}
		options_dialog.popup_centered()

	else:
		print("[WARNING] : Unimplemented feature")
