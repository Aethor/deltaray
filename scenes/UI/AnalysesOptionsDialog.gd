extends PopupDialog

var module = null

func _ready():
	get_node("VBoxContainer/HBoxContainer/OKButton").connect(
		"pressed", self, "_on_ok_click")
	get_node("VBoxContainer/HBoxContainer/CancelButton").connect(
		"pressed", self, "_on_cancel_click")
	self.connect("about_to_show", self, "_on_about_to_show")

func _on_about_to_show():
	if self.module == null:
		print("[WARNING] options dialog : module is null")
		return
	#TODO: implements arguments request here

func _on_ok_click():
	if self.module == null:
		print("[WARNING] options dialog : module is null")
		return

	var analyse_path = get_node(
		"VBoxContainer/GridContainer/AnalysePathButton").text

	if analyse_path == "":
		print("[WARNING] You must select an analyse path")
		return


	print("[WARNING] argument selection not implemented")
	self.visible = false
	get_tree().get_root().get_node("MainController").start_simulation(
		self.module["command"], 
		self.module["args"],
		get_node("VBoxContainer/GridContainer/AnalysePathButton").text,
		self.module["render"]
	)

func _on_cancel_click():
	self.visible = false
