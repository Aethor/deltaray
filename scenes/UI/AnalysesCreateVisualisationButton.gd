extends Button

func _ready():
	pass

func _pressed():
	get_tree().get_root().get_node("MainController").stop_simulation()
	get_node("../../AnalysesVisualizeModulesDialog").popup_centered()
