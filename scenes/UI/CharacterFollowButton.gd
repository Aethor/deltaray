extends Button

func _ready():
	self.connect("toggled", self, "_on_toggle")

func _on_toggle(pressed):
	if pressed:
		self.get_tree().get_root().get_node("MainController/Viewport/Rendering").follow(self.name)
		for button in self.get_parent().get_children():
			if not button.name == self.name:
				button.pressed = false
	else:
		self.get_tree().get_root().get_node("MainController/Viewport/Rendering").unfollow()