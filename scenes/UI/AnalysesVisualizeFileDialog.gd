extends FileDialog

var caller

func _ready():
	connect("file_selected", self, "_open_file")
	add_filter("*.json ; JSON files")

func set_args(caller):
	self.caller = caller

func _open_file(path):
	caller.set_selected_path(ProjectSettings.globalize_path(path))