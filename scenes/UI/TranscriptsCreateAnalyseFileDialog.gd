extends FileDialog

func _ready():
	self.connect("file_selected", self, "_open_file")

func _open_file(path):
	get_tree().get_root().get_node("MainController").create_analyse(ProjectSettings.globalize_path(path))
