extends Button

var selected_path

func _ready():
	connect("pressed", self, "_on_pressed")

func set_selected_path(path):
	self.selected_path = path
	self.set_text(path)

func _on_pressed():
	var file_dialog = get_node("../../../../AnalysesVisualizeFileDialog")
	file_dialog.set_args(self)
	file_dialog.popup_centered()
