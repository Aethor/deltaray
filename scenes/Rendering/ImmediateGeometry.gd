extends ImmediateGeometry

func _ready():
	pass

func draw_lines(datas: Array):
	self.clear()
	self.begin(Mesh.PRIMITIVE_LINES)
	for data in datas:
		self.set_color(data[2])
		self.add_vertex(data[0])
		self.add_vertex(data[1])
	self.end()