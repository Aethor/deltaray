extends Camera

var speed = 1

func _ready():
	pass

func _process(delta):
	if Input.is_action_pressed("camera_forward"):
		self.global_translate(- self.get_global_transform().basis.z)
	if Input.is_action_pressed("camera_backward"):
		self.global_translate(self.get_global_transform().basis.z)
	if Input.is_action_pressed("camera_rotate_left"):
		self.set_rotation(Vector3(self.rotation.x, self.rotation.y + delta * self.speed, self.rotation.z))
	if Input.is_action_pressed("camera_rotate_right"):
		self.set_rotation(Vector3(self.rotation.x, self.rotation.y - delta * self.speed, self.rotation.z))
	if Input.is_action_pressed("camera_rotate_up"):
		self.set_rotation(Vector3(self.rotation.x + delta * self.speed, self.rotation.y, self.rotation.z))
	if Input.is_action_pressed("camera_rotate_down"):
		self.set_rotation(Vector3(self.rotation.x - delta * self.speed, self.rotation.y, self.rotation.z))
