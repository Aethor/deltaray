extends Spatial

var followed_character = null
var character_datas = []
var relations = {}

func _ready():
	pass

func create_character(uid, x, y, z):
	var character = load("res://scenes/Rendering/Sphere.tscn").instance()
	self.add_child(character)
	character.set_name(str(uid))
	character.global_translate(Vector3(x, y, z))

	#create character labels
	var label = Label.new()
	label.text = uid
	self.get_node("2DCanvas").add_child(label)

	self.character_datas.append({"character" : character, "label" : label})

func move_character(uid, x, y, z):
	var character = self.get_node(str(uid))
	character.global_translate(Vector3(x, y, z))

func update_relation(name1, name2, update):
	if [name1, name2] in relations:
		relations[[name1, name2]] += update
	elif [name2, name1] in relations:
		relations[[name2, name1]] += update
	else:
		relations[[name1, name2]] = update

func unfollow():
	self.followed_character = null

func follow(uid):
	self.followed_character = self.get_node(str(uid))

func _process(delta):
	if not self.followed_character == null:
		var followed_pos = self.followed_character.global_transform.origin
		self.get_node("Camera").look_at_from_position(
			Vector3(followed_pos.x, followed_pos.y, followed_pos.z + 20.0),
			followed_pos,
			Vector3(0, 1, 0)
		)

	#character name drawing
	var camera = self.get_node("Camera")
	for character_data in self.character_datas:
		if not camera.is_position_behind(character_data["character"].global_transform.origin):
			character_data["label"].visible = true
			character_data["label"].rect_position = camera.unproject_position(character_data["character"].global_transform.origin)
		else:
			character_data["label"].visible = false

	#line drawing
	var datas = []
	for members in self.relations:
		datas.append(
			[
				get_node(members[0]).get_transform().origin,
				get_node(members[1]).get_transform().origin,
				Color("#ff0000") if relations[members] < 0 else Color("#00ff00")
			]
		)
	get_node("ImmediateGeometry").draw_lines(datas)

