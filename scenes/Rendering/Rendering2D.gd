extends Node2D

var characters_datas = []
var relations = {}

func _ready():
	pass


func create_character(name, x, y):
	var character = load("res://scenes/Rendering/Node.tscn").instance()
	self.add_child(character)
	character.set_name(name)
	character.get_node("NameLabel").set_text(name)
	character.global_translate(Vector2(x, y))

	self.characters_datas.append(character)

func move_character(name, x, y, z):
	var character = self.get_node(name)
	character.global_translate(Vector2(x, y))

func update_relation(name1, name2, update):
	if [name1, name2] in relations:
		relations[[name1, name2]] += update
	elif [name2, name1] in relations:
		relations[[name2, name1]] += update
	else:
		relations[[name1, name2]] = update
	update()

func _draw():
	for members in self.relations.keys():
		var strength = self.relations[members]
		draw_line(
			get_node(members[0] + "/Center").global_position,
			#get_node(members[0]).position,
			get_node(members[1] + "/Center").global_position,
			#get_node(members[1]).position,
			Color("#ff0000") if strength < 0 else Color("#00ff00"),
			abs(self.relations[members]),
			true
		)