import json, sys, re

class Character(object):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

class Interaction(object):
    def __init__(
        self,
        content: str,
        active_character: Character
    ):
        self.active_character = active_character
        self.target_characters = {}
        self.content = content
        self.polarity = None

    def is_in_interaction(self, character: Character):
        return character == self.active_character or character in self.target_characters.keys()

    def __str__(self):
        return "<Interaction : {}:{}:{}>".format(
            self.active_character.name,
            self.polarity,
            self.content)

    def __repr__(self):
        return str(self)


class Scene(object):

    def __init__(self):
        self.characters = []
        self.interactions = []

    def character_by_name(self, name: str, is_case_sensitive: bool):
        for character in self.characters:
            if is_case_sensitive:
                if character.name == name:
                    return character
            else:
                if character.name.lower() == name.lower():
                    return character
        return None

    def __len__(self):
        return len(self.characters)

    def __str__(self):
        return "<Scene : characters : {}, interactions : {}>".format(str(self.characters), str(self.interactions))


class Transcript(object):
    def __init__(self, json_data):
        self.scenes = []
        self.characters = []

        if not "movie_script" in json_data:
            print("invalid json")
            sys.exit()

        self.title = (
            json_data["movie_title"] if "movie_title" in json_data else "unknown"
        )

        for part in json_data["movie_script"]:
            if (
                part["type"] == "speech"
                and self.character_by_name(part["character"], True) is None
            ):
                self.characters.append(Character(part["character"]))

        current_part_type = None
        current_scene = Scene()

        for part in json_data["movie_script"]:
            if "type" in part and part["type"] == "location":
                if len(current_scene) != 0:
                    self.scenes.append(current_scene)
                current_scene = Scene()
                current_part_type = Scene


            elif (
                "type" in part
                and part["type"] == "speech"
                and not current_part_type is None
            ):
                active_char = self.character_by_name(part["character"], True)

                if current_scene.character_by_name(active_char.name, False) is None:
                    current_scene.characters.append(active_char)

                for sentence in list(filter(None, re.split("[.?!]", part["text"]))):
                    current_scene.interactions.append(
                        Interaction(
                            sentence,
                            active_char
                        )
                    )


    def character_by_name(self, name: str, is_case_sensitive: bool):
        for character in self.characters:
            if is_case_sensitive:
                if character.name == name:
                    return character
            else:
                if character.name.lower() == name.lower():
                    return character
        return None

    def interactions_count(self, character: Character):
        sum = 0
        for scene in self.scenes:
            for interaction in scene.interactions:
                sum += 1 if interaction.is_in_interaction(character) else 0
        return sum


    def __str__(self):
        return str([str(scene) for scene in self.scenes])

    def __repr__(self):
        return "<Transcript : " + str(self) + ">"

    def to_json(self):
        return json.dumps(
            {
                "scenes": [
                    {
                        "scene": str(scene),
                    }
                    for scene in self.scenes
                ],
                "characters": [{"name": ch.name} for ch in self.characters],
                "title": self.title,
            }
        )
