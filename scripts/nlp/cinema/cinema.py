#!/usr/bin/python3
import json, sys
from itertools import combinations

import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer

from structure import Transcript, Character, Interaction, Scene

class Relation(object):
    def __init__(self, character1: Character, character2: Character):
        self.character1 = character1
        self.character2 = character2
        self.interactions = []

    def polarity(self):
        sum = 0
        for inter in self.interactions:
            sum += (inter.target_characters[self.character1] * inter.polarity
                if self.character1 in inter.target_characters 
                else 0)
            sum += (inter.target_characters[self.character2] * inter.polarity 
                if self.character2 in inter.target_characters 
                else 0)
        return sum

    def __str__(self):
        return "<Relation : {}:{}:{}>".format(self.character1.name, self.character2.name, self.polarity())

    def __repr__(self):
        return str(self)

class RelationsSet(object):
    def __init__(self, *args):
        self.relations = {}
        for char_comb in [comb for comb in combinations(args, 2)]:
            self.relations[(char_comb[0].name, char_comb[1].name)] = Relation(char_comb[0], char_comb[1])

    def __getitem__(self, key):
        if len(key) != 2:
            raise Exception
        if (key[0], key[1]) in self.relations:
            return self.relations[(key[0], key[1])]
        if (key[1], key[0]) in self.relations:
            return self.relations[(key[1], key[0])]
        return None
       

def local_proximity_score(scene: Scene, interaction: Interaction, character: Character):
    interaction_index = scene.interactions.index(interaction)

    distances = []
    for inter in scene.interactions:
        if inter.active_character == character and inter != interaction:
            distances.append(abs(scene.interactions.index(inter) - interaction_index))

    if len(distances) == 0:
        return 0

    return sum([1/dist for dist in distances])
    
def get_distance_map(scene: Scene, interaction: Interaction):
    if not interaction in scene.interactions:
        print("error : bad interaction")
        return None
    
    if len(scene.characters) <= 1:
        return None


    character_scores = {char:0 for char in scene.characters if not char is interaction.active_character}
    for character in character_scores.keys():
        character_scores[character] += local_proximity_score(scene, interaction, character)

    return character_scores


def export_analyse(input_path: str):

    transcript = Transcript(json.loads(open(input_path, "r").read()))

    relations_set = RelationsSet(*(transcript.characters))
    sia = SentimentIntensityAnalyzer()

    to_dump = {
        "characters": [
            {
                "name": ch.name,
                "interactionsCount": transcript.interactions_count(ch)
            }
            for ch in transcript.characters
            ],
        "interactions" : []
    }

    for scene in transcript.scenes:
        for interaction in scene.interactions:
            interaction.target_characters = get_distance_map(scene, interaction)
            interaction.polarity = sia.polarity_scores(interaction.content)["compound"]
            if not interaction.target_characters is None:

                for target_char in interaction.target_characters.keys():
                    relations_set[(interaction.active_character.name, target_char.name)].interactions.append(interaction)
                to_dump["interactions"].append({
                    "activeCharacter" : interaction.active_character.name,
                    "targetCharacters" : [{
                        "name": char.name,
                        "influence": dist * interaction.polarity
                    } for char, dist in interaction.target_characters.items()]
                })

    return json.dumps(to_dump)

print(export_analyse(sys.argv[1]))