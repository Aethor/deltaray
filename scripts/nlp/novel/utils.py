import nltk

def print_progression(completion, bar_len=20):
    """

    :param completion: completion between 0 and 1
    :type completion: float
    :param bar_len: length of the progress bar (default 20)
    :type bar_len: int
    """
    fill_len = int(round(bar_len * completion))
    disp = (
        "["
        + ("=" * fill_len)
        + ("-" * (bar_len - fill_len))
        + "]"
    )
    print("{:20} {:10.2f}%".format(disp, completion * 100), end="\r")

def chunk_to_sent(chunk: nltk.tree.Tree) -> str:
    if len(chunk) == 0:
        return ""
    return " ".join([word for word, tag, block in nltk.chunk.tree2conlltags(chunk)])