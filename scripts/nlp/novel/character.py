import json, os
from enum import Enum

titles = json.loads(open(os.path.dirname(os.path.abspath(__file__)) + "/titles.json").read())

class Gender(Enum):
    """Character gender
    """
    MALE = 0
    FEMALE = 1
    UNKNOWN = 2

    @classmethod
    def get_gender(self, name: str, lcontext: str, rcontext: str) -> Enum:
        """ Try to determine the gender of an entity
        """
        lcontext = lcontext.split(" ")
        rcontext = rcontext.split(" ")
        if len(lcontext) > 0:
            if lcontext[-1] in titles["males"]:
                return Gender.MALE
            elif lcontext[-1] in titles["females"]:
                return Gender.FEMALE
        return Gender.UNKNOWN

    @classmethod
    def gender_or(self, gender1: Enum, gender2: Enum) -> Enum:
        if (not gender1 == Gender.UNKNOWN
                and not gender2 == Gender.UNKNOWN
                and not gender2 == gender1):
            return Gender.UNKNOWN
        if gender1 == Gender.MALE or gender2 == Gender.MALE:
            return Gender.MALE
        if gender1 == Gender.FEMALE or gender2 == Gender.FEMALE:
            return Gender.FEMALE
        return Gender.UNKNOWN


class Character():
    """A Character in a novel
    """

    def __init__(self, name: str, gender: Gender, occurences: int, aliases: list):
        """Character constructor
        """
        self.name = name
        self.gender = gender
        self.occurences = occurences
        self.aliases = aliases

    def add_alias(self, alias: str):
        """Add an alias to a character
        """
        self.aliases.append(alias)

    def add_occurence(self, other_occ: int):
        self.occurences += other_occ

    def __str__(self):
        return str(vars(self))

    def __repr__(self):
        return str(self)


