import re, argparse

import ebooklib
from ebooklib import epub

import nltk
nltk.download("punkt")
nltk.download("averaged_perceptron_tagger")
nltk.download("maxent_ne_chunker")
nltk.download("words")

from utils import print_progression, chunk_to_sent
from character import Gender, Character


#capture quotes with all possible utf-8 quotes
quotes_regex = re.compile(r"([\"'`‘“][^\"'`´‘’“”]+?[\"'´’”])")


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("-i", "--input-file", help="input file path", type=str)
arg_parser.add_argument(
    "-m",
    "--min-occurences-count",
    help="minimum number of occurences for a character to be considered (default : 10)",
    type=int
)
args = arg_parser.parse_args()

if not args.input_file:
    print("no input file specified. Exiting...")
    exit()
input_file = args.input_file

min_occurences_count = 10
if args.min_occurences_count:
    min_occurences_count = args.min_occurences_count


book = epub.read_epub(args.input_file)

docs = book.get_items_of_type(ebooklib.ITEM_DOCUMENT)
chapters = [
    re.sub(
        "<.*?>",
        "",
        re.sub(
            "</p>",
            "\n",
            doc.get_body_content().decode()
        )
    )
    for doc in docs if doc.is_chapter()
]


entities = {}
occurences = dict()
print("loading entities")

for i, chapter in enumerate(chapters):
    print_progression(i/len(chapters))

    sents = nltk.sent_tokenize(chapter)
    for sent in sents:
        chunks = nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sent)))
        for i, chunk in enumerate(chunks):

            if not isinstance(chunk, nltk.Tree):
                continue

            if not chunk.label() == "PERSON":
                continue

            name = " ".join([word for word, tag in chunk])
            gender = Gender.get_gender(
                name,
                chunk_to_sent(chunks[:i]),
                chunk_to_sent(chunks[i+1:])
            )

            key = (name, gender)
            if key in occurences:
                occurences[key]["occurences"] += 1
            else:
                occurences[key] = {
                    "name": name,
                    "gender": gender,
                    "occurences": 1
                }


occurences = [v for v in occurences.values()]
characters = []
assigned = []
print("performing character unification")
for i, occ in enumerate(occurences):
    print_progression(i / len(occurences))

    if i in assigned:
        continue

    character = Character(
        occ["name"],
        occ["gender"],
        occ["occurences"],
        []
    )
    characters.append(character)

    for j, other in enumerate(occurences[i + 1:]):

        if j + i + 1 in assigned:
            continue

        if not (other["gender"] == occ["gender"]
            or other["gender"] == Gender.UNKNOWN
            or occ["gender"] == Gender.UNKNOWN):

            continue

        for w1 in occ["name"].split(" "):
            for w2 in other["name"].split(" "):
                if w1 == w2:
                    character.add_alias(other["name"])
                    character.add_occurence(other["occurences"])
                    character.gender = Gender.gender_or(character.gender, other["gender"])
                    assigned.append(j + i + 1)

characters[:] = [c for c in characters if c.occurences >= min_occurences_count]