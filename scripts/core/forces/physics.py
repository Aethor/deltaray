from typing import Optional
import math


class Vector3(object):
    """
        Utility class, representing a 3D vector
        In this class, functions followed by an underscore
        (like "add_") directly modify the vector using side
        effects, while functions without that underscore 
        returns a newly created Vector
    """

    def __init__(self, x, y, z):
        """
            Constructor used to create a Vector3

            :param x: x coordinate
            :type x: float
            :param y: y coordinate
            :type y: float
            :param z: z coordinate
            :type z: float
        """
        self.x = x
        self.y = y
        self.z = z

    def equals(self, v):
        return self.x == v.x and self.y == v.y and self.z == v.z

    def distance(self, v):
        return math.sqrt(
            (self.x - v.x) ** 2 + (self.y - v.y) ** 2 + (self.z - v.z) ** 2
        )

    def is_null(self):
        return self.x == 0 and self.y == 0 and self.z == 0

    def add(self, v):
        return Vector3(self.x + v.x, self.y + v.y, self.z + v.z)

    def add_(self, v):
        self.x += v.x
        self.y += v.y
        self.z += v.z

    def sub(self, v):
        return Vector3(self.x - v.x, self.y - v.y, self.z - v.z)

    def sub_(self, v):
        self.x -= v.x
        self.y -= v.y
        self.z -= v.z

    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z

    def __add__(self, v):
        return self.add(v)

    def __radd__(self, v):
        if v == 0:
            return self
        else:
            return self.add(v)

    def __sub__(self, v):
        return self.sub(v)

    def scale(self, l):
        return Vector3(self.x * l, self.y * l, self.z * l)

    def scale_(self, l):
        self.x *= l
        self.y *= l
        self.z *= l

    def norm(self):
        return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)

    def unit_scale(self):
        if self.norm() == 0:
            return Vector3(0, 0, 0)
        return self.scale(1 / self.norm())

    def unit_scale_(self):
        if self.norm != 0:
            self.scale_(1 / self.norm())

    def to_simple_dict(self):
        return {"x": self.x, "y": self.y, "z": self.z}

    def __str__(self):
        return str(self.x) + ", " + str(self.y) + ", " + str(self.z)

    def __repr__(self):
        return str(self.x) + ", " + str(self.y) + ", " + str(self.z)



class Relation(object):
    """
        Class representing the relation between two characters
        A relation is defined by two characters and a strength
        In this module it is constructed gradually : 
        a relation is created (with strength 0),
        and the strength is updated over time
    """

    def __init__(self, character1, character2, strength):
        """
            Constructor used to create a relation between two characters

            :param character1: the first character of the relation
            :type character1: Character
            :param character1: the second character of the relation
            :type character1: Character
            :param strength: the strength of the relation
            :type strength: float
        """
        self.characters = (character1, character2)
        self.strength = strength

    def get_other(self, character):
        """
            Utility function, that will return the second character in the
            relation given the first one
            :param character: the first character of the relation
            :type character: Character
            :returns: the second character of the relation
            :rtype: Character or None
        """
        if character == self.characters[0]:
            return self.characters[1]
        elif character == self.characters[1]:
            return self.characters[0]
        return None

    def __str__(self):
        return (
            "("
            + str(self.characters[0])
            + ","
            + str(self.characters[1])
            + ")("
            + str(self.strength)
            + ")"
        )


class Interaction(object):
    """
        Class representing a single interaction between two characters
        An interaction comes from a dialog coming from a character, aimed at
        one or more character.
        The "active character" of an interaction is the character at the origin
        of the interaction, while the "target character" is one of the cahracter
        toward who the interaction is aimed at.
    """

    def __init__(self, active_character, target_character, strength):
        """
            Constructor used to create an interaction

            :param active_character: The active character of the interaction
            :type active_character: Character
            :param target_character: The target character of the interaction
            :type target_character: Character
        """
        self.active_character = active_character
        self.target_character = target_character
        self.strength = strength

    def __str__(self):
        return (
            "active character : " + "None"
            if self.active_character is None
            else self.active_character.name
            + "\ntarget characters : "
            + str([str(ch) for ch in self.target_character])
        )


class Character(object):
    """
        This class represents a character in the simulation
    """

    def __init__(self,
        name: str,
        interactionsCount: int = 0,
        mass: float = 1,
        position: Vector3 = None,
        is2D: bool = False,
        radius: float = 4,
        max_dist: float = 200
    ):
        """
            Constructor used to create a character in the simulation

            :param name: the name of the character
            :param interactionsCount: total number of interactions of this character (default : 0)
            :param mass: initial mass of the character (default : 1)
            :param position: initial position of the character (default : (0,0,0))
            :param is2D: True if the character is in a 2D world, False otherwise (default : False)
            :param radius: radius of the character (for now, the character is a sphere) (default : 4)
            :param max_dist: maximum distance to the center of the world for this character
        """
        self.name = name
        self.interactionsCount = interactionsCount
        self.is2D = is2D
        self.radius = radius
        self.max_dist = max_dist

        self.position = position if not position is None else Vector3(0,0,0)
        if self.is2D:
            self.position.z = 0

        self.mass = mass
        self.speed = Vector3(0, 0, 0)
        self.acceleration = Vector3(0, 0, 0)
        self.relations = []
        self.forces = []

    def update_relations(self, interaction: Interaction) -> Optional[Relation]:
        """
            Update the correct relation of the current character
            by adding the given interaction

            :param interaction: 
            :return: the updated Relation between the 2 character, or None if nothing was done
        """
        if interaction.active_character == self:
            relation = self.relation_by_character(interaction.target_character)
            if relation is None:
                relation = Relation(self, interaction.target_character, interaction.strength)
                self.relations.append(relation)
            else:
                relation.strength += interaction.strength
            return relation

        elif self == interaction.target_character:
            relation = self.relation_by_character(interaction.active_character)
            if relation is None:
                relation = Relation(self, interaction.active_character, interaction.strength)
                self.relations.append(relation)
            else:
                relation.strength += interaction.strength
            return relation

        return None

    def relation_by_character(self, character):
        """
            Utility function used to select a relation containing the specified character

            :param character: 
            :type character: Character
            :returns: the first relation containing the specified character
            :rtype: Relation or None
        """
        for relation in self.relations:
            if (
                relation.characters[0] == character
                or relation.characters[1] == character
            ):
                return relation
        return None

    def update(self, delta, other_characters):
        """
            Update this character's position. 
            This function is supposed to be called every frame

            :param delta: time elapsed since last frame in second
            :type delta: float
            :param other_characters: a list containing the other characters in the simulation
            :type other_characters: List[Character]
        """
        forces_sum = Vector3(0, 0, 0)

        # Gravity relative to relations
        for relation in self.relations:
            other = relation.get_other(self)
            if not other is None:
                direction_vector = other.position - self.position
                forces_sum.add_(
                    direction_vector.scale(
                        (relation.strength * self.mass * other.mass)
                        / (self.position.distance(other.position) ** 2)
                    )
                )

        # Air friction
        forces_sum.add_(self.speed.unit_scale().scale((self.speed.norm() ** 2) * 0.001 * -1))

        # Center attraction
        to_center = (Vector3(0,0,0) - self.position).unit_scale()
        dist_to_center = self.position.distance(Vector3(0,0,0))
        forces_sum_proj = to_center.scale(forces_sum.dot(to_center))
        forces_sum.add_(to_center.scale( 
            forces_sum_proj.norm()
            if dist_to_center >= self.max_dist 
            else forces_sum_proj.norm() * ( (dist_to_center) / self.max_dist)
            ))

        self.acceleration = forces_sum.scale(1 / self.mass)
        self.speed.add_(self.acceleration.scale(delta))
        self.position.add_(self.speed.scale(delta))

        for other in other_characters:
            if self.position.distance(other.position) < self.radius:
                self.position = other.position + (self.position - other.position).unit_scale().scale(self.radius)

        if self.is2D:
            self.position.z = 0

    def __str__(self):
        return self.name
