import json
import random
import argparse
import re
from http.server import HTTPServer, BaseHTTPRequestHandler
from physics import Character, Vector3, Character, Interaction


class Simulation(object):
    """
        Class representing a simulation, with multiples characters
        and their relationships.
    """

    def __init__(self,
                 path,
                 min_interactions_count=10,
                 relation_update_frequency=10,
                 is2D=False,
                 radius=4,
                 max_dist=400
                 ):
        """
            Constructor used to create a simulation.

            :param path: the path to a file representing data analysed with an NLP module from a transcript. 
            :type path: str
            :param min_interactions_count: numbers of interactions for a character to be considered
            :type min_interactions_count: int
            :param relation_update_frequency: relations between characters will be updated every 1 / relation_update_frequency frames
            :type relation_update_frequency: int
            :param is2D: determine if the simulation is in 2D or 3D
            :type is2D: boolean
            :param radius: radius of each character
            :type radius: int
            :param max_dist: max distance to center of a character (experimental)
            :type max_dist: float
        """
        raw_sim_datas = json.loads(open(path, "r").read())

        self.characters = []
        for character in raw_sim_datas["characters"]:
            if character["interactionsCount"] > min_interactions_count:
                self.characters.append(
                    Character(
                        character["name"],
                        character["interactionsCount"],
                        1,
                        Vector3(
                            random.random() * 50, random.random() * 50, random.random() * 50
                        ),
                        is2D,
                        radius,
                        max_dist
                    )
                )

        self.interactions = []
        for interaction in raw_sim_datas["interactions"]:
            active_character = self.character_by_name(
                interaction["activeCharacter"])
            if not active_character is None:
                for target_character_dict in interaction["targetCharacters"]:
                    target_character = self.character_by_name(
                        target_character_dict["name"])
                    if not target_character is None:
                        self.interactions.append(
                            Interaction(
                                active_character,
                                target_character,
                                target_character_dict["influence"]
                            )
                        )

        if len(self.interactions) == 0:
            self.current_interaction = None
            return
        self.current_interaction = self.interactions[0]

        self.time_since_last_relation_update = 0
        self.relation_update_frequency = relation_update_frequency
        self.current_frame = 0

    def __len__(self):
        """
            :return: the total number of interactions in this simulation
            :rtype: int
        """
        return len(self.interactions)

    def progression(self):
        """
            :return: the current progression of the simulation, between 0 and 1
            :rtype: float
        """
        return self.current_frame / len(self)

    def relations(self):
        processed_characters = []
        relations = []
        for character in self.characters:
            for relation in character.relations:
                if relation.get_other(character) in processed_characters:
                    continue
                relations.append(relation)
            processed_characters.append(character)
        return relations


    def relations_datas(self):
        return [
            {
                "character1": relation.characters[0],
                "character2": relation.characters[1],
                "strength": relation.strength
            }
            for relation in self.relations()
        ]
        

    def next_frame(self, delta):
        """
            :param delta: time elapsed, in seconds, since last frame
            :type delta: float

            :return: A list of dictionnary each containing 'position' and 'character' keys
            :rtype: list

            .. note:: When the simulation processed all interactions, it will always returns the same positions every frame
        """
        frame_datas = {
            "positionUpdates": [],
            "relationUpdates": []
        }
        old_positions = [
            Vector3(
                c.position.x,
                c.position.y,
                c.position.z
            )
            for c in self.characters
        ]
        if self.current_interaction != None:

            if self.time_since_last_relation_update + delta > 1 / self.relation_update_frequency:

                self.current_frame += 1

                for character in self.characters:
                    updated_relation = character.update_relations(self.current_interaction)
                    if updated_relation is None:
                        continue
                    frame_datas["relationUpdates"].append(
                        {
                            "character1": updated_relation.characters[0].name,
                            "character2": updated_relation.characters[1].name,
                            "influenceUpdate": self.current_interaction.strength
                        }
                    )

                self.time_since_last_relation_update = 0
                if self.has_next_interaction():
                    self.current_interaction = self.interactions[
                        self.interactions.index(self.current_interaction) + 1
                    ]
                else:
                    self.current_interaction = None

            else:
                self.time_since_last_relation_update += delta

            for character in self.characters:
                character.update(
                    delta, [char for char in self.characters if char != character])

        for i, character in enumerate(self.characters):
            new_position: Vector3 = character.position - old_positions[i]
            if new_position.is_null():
                continue
            frame_datas["positionUpdates"].append(
                {
                    "character": character.name,
                    "positionUpdate": {
                        "x": new_position.x,
                        "y": new_position.y,
                        "z": new_position.z,
                    }
                }
            )

        return frame_datas

    def has_next_interaction(self):
        """
            :returns: True if the simulation contains unprocessed interactions, False otherwise
            :rtype: boolean
        """
        if self.current_interaction is None:
            return False
        return not self.interactions[-1] == self.current_interaction

    def character_by_name(self, name):
        """
            :param name: name of the searched character
            :type name: str

            :returns: a character if it was found, None otherwise
            :rtype: Character or None
        """
        for character in self.characters:
            if character.name == name:
                return character
        return None


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("-s", "--http-server", action="store_true",
                        help="run the core module as an http server")
args = arg_parser.parse_args()

if not args.http_server:
    exit()


class HTTPRequestHandler(BaseHTTPRequestHandler):

    simulation = None

    def __init__(self, *args):
        BaseHTTPRequestHandler.__init__(self, *args)

    def get_url_arg(self, arg_name, arg_type):
        arg = re.match(r".*" + arg_name + r"=([^&]+).*", self.path)
        if arg is None:
            return None
        return arg_type(arg.group(1))

    def send_bad_request(self):
        self.send_response(400)
        self.end_headers()
        self.wfile.write(b"Error : invalid request format")

    def do_GET(self):
        if HTTPRequestHandler.simulation is None:
            self.send_response(409)
            self.end_headers()
            self.wfile.write(b"Error : simulation not created")
            return

        if re.match(r"/simulation/characters", self.path):
            character_datas = json.dumps({
                "positions": [
                    {
                        "name": character.name,
                        "x": character.position.x,
                        "y": character.position.y,
                        "z": character.position.z
                    }
                    for character in HTTPRequestHandler.simulation.characters
                ],
                "relations": HTTPRequestHandler.simulation.relations()
            }).encode("utf-8")
            self.send_response(200)
            self.send_header("Content-length", str(len(character_datas)))
            self.end_headers()
            self.wfile.write(character_datas)
            return

        if re.match(r"/simulation/progression", self.path):
            progression_datas = json.dumps({
                "progression": HTTPRequestHandler.simulation.progression()
            }).encode("utf-8")
            self.send_response(200)
            self.send_header("Content-length", str(len(progression_datas)))
            self.end_headers()
            self.wfile.write(progression_datas)
            return

    def do_POST(self):
        if re.match(r"/shutdown", self.path):
            self.send_response(204)
            self.end_headers()
            exit()

        if re.match(r"/simulation/next_frame.*", self.path):
            delta_match = re.match(
                r"/simulation/next_frame\?delta=([^&]+)", self.path)
            if not delta_match:
                self.send_bad_request()
                return

            delta = float(delta_match.group(1))
            frames_data = json.dumps(
                HTTPRequestHandler.simulation.next_frame(delta)).encode("utf-8")

            self.send_response(200)
            self.send_header("Content-length", str(len(frames_data)))
            self.end_headers()
            self.wfile.write(frames_data)
            return

        if re.match(r"/simulation.*", self.path):
            path = self.get_url_arg("path", str)
            if path is None:
                self.send_bad_request()
                return

            args = {}
            args["path"] = path

            min_interactions_count = self.get_url_arg(
                "min_interactions_count", int)
            relation_update_frequency = self.get_url_arg(
                "relation_update_frequency", int)
            is2D = self.get_url_arg("is2D", bool)
            radius = self.get_url_arg("radius", int)
            max_dist = self.get_url_arg("max_dist", int)

            if not min_interactions_count is None:
                args["min_interactions_count"] = min_interactions_count
            if not relation_update_frequency is None:
                args["relation_update_frequency"] = relation_update_frequency
            if not is2D is None:
                args["is2D"] = is2D
            if not radius is None:
                args["radius"] = radius
            if not max_dist is None:
                args["max_dist"] = max_dist

            HTTPRequestHandler.simulation = Simulation(**args)

            characters_datas = json.dumps(
                {
                    "relations": [],
                    "positions": [
                        {
                            "character": character.name,
                            "position": {
                                "x": character.position.x,
                                "y": character.position.y,
                                "z": character.position.z,
                            }
                        }
                        for character in HTTPRequestHandler.simulation.characters
                    ]
                }
            ).encode("utf-8")
            self.send_response(201)
            self.send_header("Content-length", str(len(characters_datas)))
            self.end_headers()
            self.wfile.write(characters_datas)
            return

        self.send_response(404)
        error_message = "Resource not found".encode("utf-8")
        self.send_header("Content-length", str(len(error_message)))
        self.end_headers()
        self.wfile.write(error_message)


httpd = HTTPServer(("localhost", 8000), HTTPRequestHandler)
httpd.serve_forever()
