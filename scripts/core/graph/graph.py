import json
import argparse
import re
from http.server import HTTPServer

from simulation import Simulation
from server import HTTPRequestHandler

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument(
    "-s", "--server", help="server mode", action="store_true")
arg_parser.add_argument("-p", "--server-port",
                        help="specify server port (default 8000)", type=int)
args = arg_parser.parse_args()

if not args.server:
    exit()

server_port = 8000
if args.server_port:
    server_port = args.server_port

httpd = HTTPServer(("localhost", server_port), HTTPRequestHandler)
httpd.serve_forever()
