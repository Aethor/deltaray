import json, random, traceback
from typing import List, Tuple, Dict


class Node(object):

    def __init__(self, name: str, position: Tuple[float, float]):
        self.name = name
        self.position = position


class Interaction(object):
    def __init__(self, active_node: Node, targets_datas: List[Tuple[Node, float]]):
        """ Create a new interaction
        :param active_node: The active character of the interaction
        :param targets_datas: A list of datas representing the target
        characters of an interaction and the strength of that interaction
        on each character
        """
        self.active_node = active_node
        self.targets_datas = targets_datas


class Edge(object):
    def __init__(self, node1: Node, node2: Node, strength: float):
        self.node1 = node1
        self.node2 = node2
        self.strength = strength


class Simulation(object):
    """
        Class representing a simulation, with multiples characters
        and their relationships.
    """

    def __init__(
        self,
        path: str,
        min_interactions_count: int = 10,
        update_frequency: int = 10
    ):
        loaded_datas = json.loads(open(path).read())

        self.update_frequency = update_frequency

        try:
            self.nodes = {
                character["name"]: Node(
                    character["name"], (random.random() * 1500, random.random() * 1000))
                for character in loaded_datas["characters"]
                if character["interactionsCount"] > min_interactions_count
            }

            self.interactions = []
            for interaction in loaded_datas["interactions"]:

                if not interaction["activeCharacter"] in self.nodes:
                    continue

                targets_datas = [
                    (self.nodes[character_datas["name"]],
                     character_datas["influence"])
                    for character_datas in interaction["targetCharacters"]
                    if character_datas["name"] in self.nodes
                ]
                if len(targets_datas) == 0:
                    continue

                self.interactions.append(Interaction(
                    self.nodes[interaction["activeCharacter"]],
                    targets_datas
                ))

        except Exception as e:
            print(e)
            traceback.print_exc()
            print("Invalid input file")
            exit()

        self.edges = {}

        if len(self.interactions) == 0:
            self.current_interaction = None
        else:
            self.current_interaction = self.interactions[0]

        self.time_since_last_update = 0

    def edge(self, node1: Node, node2: Node) -> Edge or None:
        if (node1.name, node2.name) in self.edges:
            return self.edges[(node1.name, node2.name)]
        if (node2.name, node1.name) in self.edges:
            return self.edges[(node2.name, node1.name)]
        return None

    def update_relation(self, node1: Node, node2: Node, update: float):
        edge = self.edge(node1, node2)
        if edge != None:
            edge.strength += update
        else:
            self.edges[(node1.name, node2.name)] = Edge(node1, node2, update)

    def nodes_datas(self):
        datas = {
            "relations": [],
            "positions": []
        }
        for node in self.nodes.values():
            datas["positions"].append({
                "character": node.name,
                "position": {
                    "x": node.position[0],
                    "y": node.position[1]
                }
            })
        for edge in self.edges.values():
            datas["relations"].append({
                "character1": edge.node1.name,
                "character2": edge.node2.name,
                "strength": edge.strength
            })
        return datas

    def next_frame(self, delta: float):
        frame_datas = {
            "relationUpdates": [],
            "positionUpdates": []
        }

        self.time_since_last_update += delta
        if not self.time_since_last_update > (1 / self.update_frequency):
            return frame_datas

        if self.current_interaction == None:
            return frame_datas

        for target_datas in self.current_interaction.targets_datas:
            self.update_relation(
                self.current_interaction.active_node, target_datas[0], target_datas[1])

            frame_datas["relationUpdates"].append({
                "character1": self.current_interaction.active_node.name,
                "character2": target_datas[0].name,
                "influenceUpdate": target_datas[1]
            })

        if self.has_next_interaction():
            self.current_interaction = self.interactions[self.interactions.index(
                self.current_interaction) + 1]
        self.time_since_last_update = 0
        return frame_datas

    def has_next_interaction(self):
        return self.current_interaction != None and self.current_interaction != self.interactions[-1]

    def progression(self):
        return self.interactions.index(self.current_interaction) / len(self.interactions)