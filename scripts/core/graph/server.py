import re, json
from http.server import BaseHTTPRequestHandler
from typing import Union

from simulation import Simulation


class HTTPRequestHandler(BaseHTTPRequestHandler):

    simulation: Union[Simulation, None] = None

    def __init__(self, *args):
        BaseHTTPRequestHandler.__init__(self, *args)

    def get_url_arg(self, arg_name, arg_type):
        arg = re.match(r".*" + arg_name + r"=([^&]+).*", self.path)
        if arg is None:
            return None
        return arg_type(arg.group(1))

    def send_bad_request(self):
        self.send_response(400)
        self.end_headers()
        self.wfile.write(b"Error : invalid request format")

    def do_GET(self):
        if HTTPRequestHandler.simulation is None:
            self.send_response(409)
            self.end_headers()
            self.wfile.write(b"Error : simulation not created")
            return

        if re.match(r"/simulation/progression", self.path):
            progression_datas = json.dumps({
                "progression": HTTPRequestHandler.simulation.progression()
            }).encode("utf-8")
            self.send_response(200)
            self.send_header("Content-length", str(len(progression_datas)))
            self.end_headers()
            self.wfile.write(progression_datas)
            return

        if re.match(r"/simulation/characters", self.path):
            characters_datas = json.dumps(
                HTTPRequestHandler.simulation.nodes_datas()
            ).encode("utf-8")
            self.send_response(200)
            self.send_header("Content-length", str(len(characters_datas)))
            self.end_headers()
            self.wfile.write(progression_datas)
            return

        self.send_bad_request()

    def do_POST(self):
        if re.match(r"/shutdown", self.path):
            self.send_response(204)
            self.end_headers()
            exit()

        if re.match(r"/simulation/next_frame.*", self.path):
            delta_match = re.match(
                r"/simulation/next_frame\?delta=([^&]+)", self.path)
            if not delta_match:
                self.send_bad_request()
                return

            delta = float(delta_match.group(1))
            frames_data = json.dumps(
                HTTPRequestHandler.simulation.next_frame(delta)).encode("utf-8")

            self.send_response(200)
            self.send_header("Content-length", str(len(frames_data)))
            self.end_headers()
            self.wfile.write(frames_data)
            return

        if re.match(r"/simulation\?.*", self.path):
            path = self.get_url_arg("path", str)
            if path is None:
                self.send_bad_request()
                return

            args = {}
            args["path"] = path

            min_interactions_count = self.get_url_arg(
                "min_interactions_count", int)
            relation_update_frequency = self.get_url_arg(
                "relation_update_frequency", int)

            if not min_interactions_count is None:
                args["min_interactions_count"] = min_interactions_count
            if not relation_update_frequency is None:
                args["relation_update_frequency"] = relation_update_frequency

            HTTPRequestHandler.simulation = Simulation(**args)

            characters_datas = json.dumps(
                HTTPRequestHandler.simulation.nodes_datas()).encode("utf-8")

            self.send_response(201)
            self.send_header("Content-length", str(len(characters_datas)))
            self.end_headers()
            self.wfile.write(characters_datas)
            return

        self.send_response(404)
        error_message = "Resource not found".encode("utf-8")
        self.send_header("Content-length", str(len(error_message)))
        self.end_headers()
        self.wfile.write(error_message)
